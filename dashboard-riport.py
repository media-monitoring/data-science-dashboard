#!/usr/bin/env python
# coding: utf-8


from datetime import date, timedelta, datetime, time
start_time = datetime.now()
print("Start time: ", str(start_time)[:-7])
print(str(datetime.now())[:-7],' Imports..')
import pandas as pd
import seaborn as sns
import yfinance as yf
import matplotlib.pyplot as plt
from deep_translator import GoogleTranslator
import openai
from bertopic import BERTopic
from bertopic.representation import OpenAI #KeyBERTInspired
from transformers import pipeline,  AutoTokenizer,MegatronBertForSequenceClassification
from wordcloud import WordCloud
from collections import Counter
from nltk.corpus import stopwords
from nltk import word_tokenize, sent_tokenize
from textwrap import wrap
import numpy as np
import matplotlib.dates as mdates
punctuation="!\”#$%&'()*+,-./:;?@[\]^_`{|}~-"
import unidecode
import simplemma
from wordcloud import WordCloud
from PIL import Image
import os
import glob
import shutil
import warnings
from fpdf import FPDF
import pypdfium2 as pdfium
import cv2
import win32com.client as win32
from numba.core.errors import NumbaDeprecationWarning, NumbaPendingDeprecationWarning


warnings.filterwarnings(action='ignore', category=NumbaDeprecationWarning)
warnings.filterwarnings(action='ignore', category=NumbaPendingDeprecationWarning)

warnings.filterwarnings(
    action='ignore')
stopwords = stopwords.words('hungarian')
add_stopwords=['Címkék','százalékos', 'százalék','százaléka', 'százalékkal','mondta','ban','idén', 'akár','korábban', 'továbbra','miatt', 'közölte','például', 'két', 'ben', 'magyar', 'egy', 'hogy', 'tette', 'Hozzátette', 'Bár', 'ugyanakkor', 'miközben', 'jelenleg', 'Getty Images', 'címlapkép', 'Getty', 'Images']
stopwords.extend(add_stopwords)

print(str(datetime.now())[:-7],' Delete existing images..')
source=r'I:\Moni\Codes\news-monitoring\dashboard'
destination = r'I:\Moni\Codes\news-monitoring\dashboard\images'

allfiles = glob.glob(os.path.join(destination, '*'))
for f in allfiles:
    os.chmod(f,0o777)
    os.remove(f)
       
oldfiles = glob.glob(os.path.join(source, '*db.png'), recursive=True)

print("Files to move", oldfiles)
for file_path in oldfiles:
    dst_path = os.path.join(destination)
    shutil.copy2(src=file_path, dst=dst_path)
    print("Moved {} -> {}".format(file_path, dst_path))
    os.remove(file_path)
    
print(str(datetime.now())[:-7], ' Loading data..')


df0=pd.read_csv('I:/Moni/Codes/news-monitoring/analyze/portfolio_hist.csv', index_col=0)
df1=pd.read_csv('I:/Moni/Codes/news-monitoring/analyze/vg_hist.csv', index_col=0)
df2=pd.read_csv('I:/Moni/Codes/news-monitoring/analyze/mfor_hist.csv', index_col=0)
df3=pd.read_csv('I:/Moni/Codes/news-monitoring/analyze/napi_hist.csv', index_col=0)
df=pd.concat([df0,df1,df2,df3])
df=df.convert_dtypes()
df['Date']=pd.to_datetime(df['Date'])
df.sort_values('Date', inplace=True, ascending=False)
df.drop_duplicates(subset='Link', inplace=True)
df.reset_index(inplace=True, drop=True)
df['Text']=df['Text'].fillna('')
df=df[df['Date']>='2022-09-27']
df['textlen']=df['Text'].apply(lambda x: len(x))
df['day']=df['Date'].apply(lambda x: x.strftime('%Y-%m-%d'))
df['day']=pd.to_datetime(df['day'])


# ## Az elmúlt hét top témái


print(str(datetime.now())[:-7], ' Top 10 topic..')

data_topics=df[df['Date']>=(datetime.today()+timedelta(days=-7))]
data_topics[" "]=" "
data_topics['Content']=data_topics['Title']+data_topics[" "]+data_topics['Text']
data_topics['Content']=data_topics['Content'].apply(lambda x: x.replace('\xa0', ' ').replace('\n','').replace('$(','').replace('(ex); } });',''))
data_topics['Content']=data_topics.Content.str.replace(r"function(.*)console.error", r' ')


openai.api_key="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"
representation_model = OpenAI(model="gpt-3.5-turbo", chat=True, delay_in_seconds=21)
topic_model_1w = BERTopic(language="hungarian", representation_model=representation_model)

topic_data=data_topics['Content'].to_list()

topics, probs = topic_model_1w.fit_transform(topic_data)
results=topic_model_1w.get_topic_info()

ts=GoogleTranslator(source='en', target='hu')  

def translate(x):
    try:
        x_en=[ts.translate(sent) for sent in x]
    except Exception:
        x_en=x
    return x_en

results['Representation']=[translate(row) for row in results['Representation']]





i=1
len("\n".join(wrap(str(i)+'. '+results['Representation'][i][0].split('(')[0].strip()+' ('+str(results['Count'][i])+' cikk)'.capitalize(), 40)))


x, y = np.ogrid[:300, :300]
mask = (x - 150) ** 2 + (y - 150) ** 2 > 130 ** 2
mask = 255 * mask.astype(int)

figure, axis = plt.subplots(2, 5, figsize=(25, 14))

# For Sine Function

for i in range(1,6):
    wc = WordCloud(background_color='white', stopwords=stopwords, width=500, height=500, mask=mask,
                   max_words=100,  min_word_length=3,  colormap='viridis').generate(' .'.join(results['Representative_Docs'][i])) 
    axis[0, i-1].imshow(wc, interpolation = "bilinear")
    title="\n".join(wrap(str(i)+'. '+results['Representation'][i][0].split('(')[0].strip()+' ('+str(results['Count'][i])+' cikk)'.capitalize(), 40))
    if title.count("\n")==0:
        axis[0, i-1].set_title(title, fontsize=17, pad=60)
    elif title.count("\n")==1:
        axis[0, i-1].set_title(title, fontsize=17, pad=40)
    elif title.count("\n")==2:
        axis[0, i-1].set_title(title, fontsize=17, pad=20)
    else:
        axis[0, i-1].set_title(title, fontsize=17, pad=0)
    axis[0, i-1].axis('off')
    # axis.set_xticks([])
    # axis.set_yticks([])


for i in range(6,11):
    wc = WordCloud(background_color='white', stopwords=stopwords, width=500, height=500, mask=mask,
                   max_words=100,  min_word_length=3, colormap='viridis').generate(' .'.join(results['Representative_Docs'][i]))     
    axis[1, i-6].imshow(wc,interpolation = "bilinear")

    title="\n".join(wrap(str(i)+'. '+results['Representation'][i][0].split('(')[0].strip()+' ('+str(results['Count'][i])+' cikk)'.capitalize(), 40))
    if title.count('\n')==1:
        axis[1, i-6].set_title(title, fontsize=17, pad=40)
    elif title.count('\n') ==0:
        axis[1, i-6].set_title(title, fontsize=17, pad=60)
    elif title.count('\n') ==2:
        axis[1, i-6].set_title(title, fontsize=17, pad=20)
    else:
        axis[1, i-6].set_title(title, fontsize=17, pad=0)
    axis[1, i-6].axis('off')
    # axis.set_xticks([])
    # axis.set_yticks([])

plt.tight_layout()
plt.savefig(r'I:\Moni\Codes\news-monitoring\dashboard\top10_topik_db.png', bbox_inches='tight')


# ## Releváns témák időbeli alakulása

print(str(datetime.now())[:-7], ' Topic evolution..')

topic_model_ts=BERTopic.load(r"I:\MONI\Codes\news-monitoring\dashboard\BERTopic_final")
daily_topics_hist=pd.read_csv(r"I:\MONI\Codes\news-monitoring\dashboard\daily_topics.csv")
start=daily_topics_hist['day'][-1:] .values[0]                             
data_ts=df[df['Date']>=start] #new_data=last few days
data_ts[" "]=" "
data_ts['Content']=data_ts['Title']+data_ts[" "]+data_ts['Text']
data_ts['Content']=data_ts['Content'].apply(lambda x: x.replace('\xa0', ' ').replace('\n','').replace('$(','').replace('(ex); } });',''))
data_ts['Content']=data_ts.Content.str.replace(r"function(.*)console.error", r' ')
data_ts['Content']=data_ts['Content'].apply(lambda x: x.split('A jelen oldalon található információk és elemzések a szerzők magánvéleményét tükrözik.')[0])

topic_data_ts=data_ts['Content'].to_list()
topics_ts, probs_ts = topic_model_ts.transform(topic_data_ts)  #new data

topic_forint=[]
topic_EU=[]
topic_bankcsod=[]
topic_inflacio=[]
topic_koltsegvetes=[]
topic_kamatdontes=[]

for i in range(0, len(topic_data_ts)):
    
    topic_forint.append(probs_ts[i][0])
    
    topic_EU.append(probs_ts[i][6])
    
    topic_bankcsod.append(probs_ts[i][14])
    
    topic_inflacio.append(probs_ts[i][25])
    
    topic_koltsegvetes.append(probs_ts[i][35])
    
    topic_kamatdontes.append(probs_ts[i][30])
    
data_ts['forint']=topic_forint
data_ts['EU']=topic_EU
data_ts['bankcsőd']=topic_bankcsod
data_ts['infláció']=topic_inflacio
data_ts['költségvetés']=topic_koltsegvetes
data_ts['kamatdöntés']=topic_kamatdontes

data_ts['forint']=data_ts['forint']*data_ts['textlen']
data_ts['EU']=data_ts['EU']*data_ts['textlen']
data_ts['bankcsőd']=data_ts['bankcsőd']*data_ts['textlen']
data_ts['infláció']=data_ts['infláció']*data_ts['textlen']
data_ts['költségvetés']=data_ts['költségvetés']*data_ts['textlen']
data_ts['kamatdöntés']=data_ts['kamatdöntés']*data_ts['textlen']
daily_topics_new=data_ts.groupby('day').sum()
daily_topics_new.reset_index(inplace=True)
daily_topics_all=pd.concat([daily_topics_new, daily_topics_hist])
daily_topics_all['day'] =  pd.to_datetime(daily_topics_all['day'], format='%Y-%m-%d')
#daily_topics_all.drop('index', inplace=True, axis=1)
daily_topics_all.drop_duplicates('day', inplace=True)
daily_topics_all.sort_values('day', inplace=True)
daily_topics_all.set_index('day', inplace=True)
daily_topics_all.to_csv(r"I:\MONI\Codes\news-monitoring\dashboard\daily_topics.csv")

dayofweek=daily_topics_all.last_valid_index().dayofweek
def get_freq(dow):
    if dow==0:
        freq='W-MON'
    elif dow==1:
        freq='W-TUE'
    elif dow==2:
        freq='W-WED'
    elif dow==3:
        freq='W-THU'
    elif dow==4:
        freq='W-FRI'
    elif dow==5:
        freq='W-SAT'
    elif dow==6:
        freq='W-SUN'
    return freq

freq=get_freq(dayofweek)
res = daily_topics_all.groupby(pd.Grouper(freq=freq,  origin='end')).sum()[1:]





figure, host = plt.subplots(1, 2, figsize=(35, 10))

host[0].set_facecolor("#383d41")


host[0].set_ylabel("Becsült heti karaktermennyiség (ezer)", fontsize=20,labelpad=10)
par0=host[0].twinx()
host[0].yaxis.label.set_backgroundcolor('#383d41')
par0.yaxis.label.set_backgroundcolor('#383d41')

p1, = host[0].plot(res.index, res.forint/1000,color='white', linewidth=6,alpha=0.8,label='Forint')
p2, = par0.plot(res.index, res.kamatdöntés/1000,color='#01FFC3', linewidth=6,alpha=0.8,label='Monetáris politika (jobb t.)')
p5, = host[0].plot(res.index, res.infláció/1000,color='#dcff30',linewidth=6,alpha=0.8,label='Infláció')

host[0].scatter(res.index[-1:], res.forint[-1:]/1000, c='lightcoral', marker='o', linewidths=10)
par0.scatter(res.index[-1:], res.kamatdöntés[-1:]/1000, c='lightcoral', marker='o', linewidths=10)
host[0].scatter(res.index[-1:], res.infláció[-1:]/1000, c='lightcoral', marker='o', linewidths=10)

host[0].set_ylim(-10,70)
par0.set_ylim(0,100)
lns = [p1,p5,p2 ]
host[0].legend(handles=lns, loc='upper left', fontsize=20, facecolor='#383d41', labelcolor='white')

host[0].yaxis.label.set_color('white')
par0.yaxis.label.set_color('white')

host[0].xaxis.set_major_locator(mdates.DayLocator(interval=7)) 
host[0].tick_params(axis='x', which='major', labelsize=18)
host[0].tick_params(axis='y', which='major', labelsize=18)
par0.tick_params(axis='y', which='major', labelsize=18)

host[0].set_xticklabels(res.index, rotation=90)
plt.tight_layout()
####

host[1].set_facecolor("#383d41")

plt.xticks(rotation = 90)    
plt.xticks(res.index)

host[1].set_ylabel("Becsült heti karaktermennyiség (ezer)", fontsize=20,labelpad=10)

par1=host[1].twinx()
host[1].yaxis.label.set_backgroundcolor('#383d41')
par1.yaxis.label.set_backgroundcolor('#383d41')

p3, = host[1].plot(res.index, res.EU/1000,color='#dcff30',linewidth=6,alpha=0.8,label='EU-pénzek')
p4, = host[1].plot(res.index, res.bankcsőd/1000, color='white',linewidth=6,alpha=0.8, label='Bankcsőd')
p6, = par1.plot(res.index, res.költségvetés/1000, color='#01FFC3',linewidth=6,alpha=0.8, label='Költségvetés (jobb t.)')

host[1].scatter(res.index[-1:], res.EU[-1:]/1000, c='lightcoral', marker='o', linewidths=12)
par1.scatter(res.index[-1:], res.költségvetés[-1:]/1000, c='lightcoral', marker='o', linewidths=12)
host[1].scatter(res.index[-1:], res.bankcsőd[-1:]/1000, c='lightcoral', marker='o', linewidths=12)

host[1].set_ylim(0,60)
par1.set_ylim(-10,50)
lns = [p4,p3, p6]
host[1].legend(handles=lns, loc='upper right', fontsize=20, facecolor='#383d41', labelcolor='white')



host[1].yaxis.label.set_color('white')
par1.yaxis.label.set_color('white')

host[1].xaxis.set_major_locator(mdates.DayLocator(interval=7)) 
host[1].tick_params(axis='x', which='major', labelsize=18)
host[1].tick_params(axis='y', which='major', labelsize=18)
par1.tick_params(axis='y', which='major', labelsize=18)

host[1].set_xticklabels(res.index, rotation=90)
plt.xticks(rotation = 90)    
plt.xticks(res.index)
par1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
par0.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
plt.tight_layout(pad=5.0)
plt.savefig(r"I:\MONI\Codes\news-monitoring\dashboard\topic_evolution_db.png", bbox_inches='tight')


# ## Forint szentiment index

print(str(datetime.now())[:-7], ' Forint sentiment..')

tokenizer = AutoTokenizer.from_pretrained("NYTK/husst-puli-bert-large-hungarian")
model = MegatronBertForSequenceClassification.from_pretrained("NYTK/husst-puli-bert-large-hungarian")

classifier = pipeline(task='sentiment-analysis', model=model, tokenizer=tokenizer)




df_forint=df.query('day.dt.dayofweek <5')
unnep=[pd.to_datetime('2022-10-31'), 
       pd.to_datetime('2022-11-01'),
       pd.to_datetime('2022-12-26'),
       pd.to_datetime('2023-03-15'),
       pd.to_datetime('2023-04-07'),
       pd.to_datetime('2023-04-10'),
       pd.to_datetime('2023-05-01'),
       pd.to_datetime('2023-05-29')]
df_forint=df_forint[~df_forint['day'].isin(unnep)]
forint_hist=pd.read_csv(r"I:\MONI\Codes\news-monitoring\dashboard\forint.csv")
last_day=forint_hist.iloc[-1]['day']
data_forint=df_forint[df_forint['Date']>=last_day]
data_forint[" "]=" "
data_forint['Content']=data_forint['Title']+data_forint[" "]+data_forint['Text']
data_forint['Content']=data_forint['Content'].apply(lambda x: x.replace('\xa0', ' ').replace('\n','').replace('$(','').replace('(ex); } });',''))
data_forint['Content']=data_forint.Content.str.replace(r"function(.*)console.error", r' ')
data_forint['Content']=data_forint['Content'].apply(lambda x: x.split('A jelen oldalon található információk és elemzések a szerzők magánvéleményét tükrözik.')[0])
forint=data_forint[data_forint["Content"].str.contains("forint árfolyam|euró-forint|euróárfolyam|forintárfolyam|forint euró|forint az euróval|EUR/HUF|forinterősödés|forintgyengülés")]

#data_forint= data_forint['Content'].apply(lambda x: word_tokenize(x))

def classify(x):
    try:
        x_raw=[classifier(sent) for sent in x]
    except Exception:
        x_raw=None
    return x_raw

def get_sentiment(x):
    try:
        neg_sum=0
        pos_sum=0
        all_sum=0
        for j in range(0,len(x)):
            if x[j][0]['label']=='LABEL_0':
                     neg_sum+=x[j][0]['score']
                     all_sum+=x[j][0]['score']
            elif x[j][0]['label']=='LABEL_2':
                     pos_sum+=x[j][0]['score']
                     all_sum+=x[j][0]['score']
            else:
                     all_sum+=x[j][0]['score']

        return (pos_sum-neg_sum)/all_sum
    except Exception:
        pass





forint['Content_sent']=forint['Content'].apply(lambda x: sent_tokenize(x))
forint_data=forint['Content_sent'].values
forint['raw_sentiment']=[classify(elem) for elem in forint_data]
forint['sentiment']=forint['raw_sentiment'].apply(lambda x: get_sentiment(x))




forint=pd.concat([forint,forint_hist])
forint['Date'] =  pd.to_datetime(forint['Date'])
forint['day'] =  pd.to_datetime(forint['day'], format='%Y-%m-%d')
forint.drop('Unnamed: 0', inplace=True, axis=1)
forint.drop_duplicates('Link', inplace=True)
forint.sort_values('Date', inplace=True)
forint.to_csv(r"I:\MONI\Codes\news-monitoring\dashboard\forint.csv")




eurhuf=yf.download("EURHUF=X", start=pd.to_datetime('2022-09-27'), end=date.today()+timedelta(days=1), interval='1d')
eurhuf=eurhuf.shift(periods=-1)
eurhuf.reset_index(inplace=True)
eurhuf['Date']=pd.to_datetime(eurhuf['Date'])
eurhuf=eurhuf[~eurhuf['Date'].isin(unnep)]
eurhuf=eurhuf[:-1]
daily_forint= forint.groupby('day').sum('sentiment')
daily_forint.reset_index()

eurhuf=eurhuf.rename(columns= {'Date': 'day'})
#daily_forint.set_index('day', inplace=True)

daily_forint=pd.merge(left=daily_forint, right=eurhuf[['day', 'Close']],  on='day')
#daily_forint['EURHUF']=eurhuf['Close'].values

daily_forint.reset_index(inplace=True)

daily_forint['neg_sent']=daily_forint['sentiment'].apply(lambda x: x*-1)
daily_forint['neg_sent_rolling']=daily_forint['neg_sent'].rolling(2).sum()
#daily_forint.to_csv("I:\MONI\Codes\news-monitoring\dashboard\daily_forint.csv")
forint['day']=pd.to_datetime(forint['day'])
intra_sent=forint[['day', 'sentiment']]
eurhuf_chart=daily_forint[daily_forint['day']>=pd.to_datetime(datetime.today().date()+timedelta(days=-32))]
intra_chart=intra_sent[intra_sent['day']>=pd.to_datetime(datetime.today().date()+timedelta(days=-32))]




palette=sns.light_palette("#14c49b",n_colors=30)
result = pd.merge(left=intra_chart, right=(intra_chart.groupby('day').mean()['sentiment']*-1).reset_index(),  on='day')
result['sentiment_x']=result['sentiment_x']*-1


# In[677]:


fig, host = plt.subplots(1, 2, figsize=(35, 10))


host[0].set_facecolor("#383d41")
host[0].xaxis.set_major_locator(mdates.DayLocator(interval=7)) 
host[0].tick_params(axis='x', which='major', labelsize=20)
  
par0 = host[0].twinx()


host[0].set_ylim(-2, 14)
par0.set_ylim(330,440)

    

host[0].set_ylabel("+       Forint szentiment       -", fontsize=20)
par0.set_ylabel("EURHUF", fontsize=20, rotation=-90, labelpad=30)
par0.spines['right'].set_color('lightcoral')
par0.spines['right'].set_linewidth(5)

host[0].yaxis.label.set_backgroundcolor('#383d41')
par0.yaxis.label.set_backgroundcolor('#383d41')
p1=host[0].fill_between(daily_forint['day'].dt.strftime('%Y-%m-%d'),
                    daily_forint['neg_sent_rolling'], 
                     daily_forint['neg_sent_rolling'].mean(),
                     color='#01FFC3',alpha=0.8,label='Forint szentiment')


p2, = par0.plot(daily_forint['day'].dt.strftime('%Y-%m-%d'),daily_forint['Close'], color="lightcoral" ,label='EURHUF',linewidth=3)

lns = [p1,p2]
host[0].legend(handles=lns, loc='upper right', fontsize=20, facecolor='#383d41', labelcolor='white')

host[0].yaxis.label.set_color('#01FFC3')
par0.yaxis.label.set_color('lightcoral')

host[0].xaxis.set_major_locator(mdates.DayLocator(interval=7)) 
host[0].tick_params(axis='x', which='major', labelsize=18, rotation=90)
host[0].tick_params(axis='y', which='major', labelsize=18)
par0.tick_params(axis='y', which='major', labelsize=18)

#####

PROPS = {
    'boxprops':{ 'edgecolor':'white'},
    'medianprops':{'color':'#383d41'},
    'whiskerprops':{'color':'white'},
    'capprops':{'color':'white'},
    'meanprops':{"marker":"o",
               "markerfacecolor":"white", 
               "markeredgecolor":"black",
              "markersize":"10"}
}

host[1].set_facecolor("#383d41")

  
par1 = host[1].twinx()

par1.set_ylabel("EURHUF", fontsize=20, rotation=-90, labelpad=30)
par1.spines['right'].set_color('lightcoral')
par1.spines['right'].set_linewidth(5)

host[1].yaxis.label.set_backgroundcolor('#383d41')
par1.yaxis.label.set_backgroundcolor('#383d41')

sns.boxplot(data=result,x=result['day'], y=result['sentiment_x'],hue=result['sentiment_y'],dodge=False,
            palette=palette,showfliers=False,ax=host[1], showmeans=True, **PROPS)

p3=host[0].fill_between([],[],[],color='#01FFC3',alpha=0.8,label='Forint szentiment')

p4, = par1.plot(eurhuf_chart['day'].dt.strftime('%Y-%m-%d'),eurhuf_chart['Close'], color="lightcoral" ,label='EURHUF',linewidth=3)

lns = [p3,p4]
host[1].legend(handles=lns, loc='upper left', fontsize=20, facecolor='#383d41', labelcolor='white')
host[1].set_ylabel("+       Forint szentiment       -", fontsize=20)
host[1].yaxis.label.set_color('#01FFC3')
par1.yaxis.label.set_color('lightcoral')


host[1].tick_params(axis='x', which='major', labelsize=18, rotation=90)
host[1].tick_params(axis='y', which='major', labelsize=18)
par1.tick_params(axis='y', which='major', labelsize=18)
host[1].set_xlabel("", fontsize=18)
host[1].set_xticklabels(eurhuf_chart['day'].dt.strftime('%m-%d'), rotation=90)


plt.tight_layout(pad=5)
plt.savefig(r'I:\Moni\Codes\news-monitoring\dashboard\forint_sentiment_db.png',bbox_inches='tight')

# ## Forint témájú cikkek szófelhője

print(str(datetime.now())[:-7], ' Forint word cloud..')


add_stopwords=['forint', 'százalék', 'magyar', 'is', 'ha']
stopwords.extend(add_stopwords)




forint_wc=forint[forint['Date']>=(datetime.today()+timedelta(days=-7))]['Content']
text=''

for t in forint_wc:
    text+=t
    



# mask_forint = np.array(Image.open(r"I:\MONI\Codes\news-monitoring\dashboard\mask_huf.png"))
# wc = WordCloud(background_color="white",collocation_threshold=20,
#                 stopwords=stopwords, mask=mask_forint,  max_words=500,min_word_length=3, include_numbers=True,min_font_size=3,scale=5,
#                 ).generate(text)
# plt.figure(figsize=(24,12))
# plt.axis('off')
# plt.imshow(wc)
# plt.savefig(r"I:\Moni\Codes\news-monitoring\dashboard\forint_wc_db.png",bbox_inches='tight', pad_inches=1)




mask_forint = np.array(Image.open(r"I:\MONI\Codes\news-monitoring\dashboard\mask_forint.png"))
wc = WordCloud(background_color="white", max_words=800,min_word_length=3,min_font_size=1,scale=20,
                stopwords=stopwords, mask=mask_forint, include_numbers=True).generate(text)
plt.figure(figsize=(25,10))
plt.axis('off')
plt.imshow(wc)
plt.savefig(r"I:\Moni\Codes\news-monitoring\dashboard\forint_wc1_db.png",bbox_inches='tight')

print(str(datetime.now())[:-7], ' Create PDF..')
pdf = FPDF()
pdf.add_page()
today=datetime.now().strftime("%Y-%m-%d")
today_nicer=datetime.now().strftime("%Y. %m. %d.")

pdf.add_font('OpenSans','','I:/MONI/Codes/google-trends/OpenSans-Regular.ttf',uni=True)
pdf.add_font('OpenSans','B','I:/MONI/Codes/google-trends/OpenSans-Bold.ttf',uni=True)
pdf.set_font('OpenSans','B', 10)
pdf.set_text_color(r= 0, g= 0, b = 0)
pdf.cell(0,10, 'Monetáris Politika és Pénzpiaci Elemzés igazgatóság',  align='L')
pdf.cell(0,10,today_nicer, align='R',ln=1)

pdf.set_font('OpenSans', 'B', 18)
pdf.set_fill_color(r= 219, g= 219, b = 219)
#pdf.set_draw_color(r= 196, g= 196, b = 196)
pdf.cell(0,3, '', border=0, align='C', fill=True, ln=1)
pdf.cell(0,10, 'DATA SCIENCE DASHBOARD', border=0, align='C', fill=True, ln=1)
pdf.set_font('OpenSans','B', 6)
pdf.cell(0,5,'PÉNZÜGYI-GAZDASÁGI TÉMÁJÚ HÍREK ELEMZÉSE',fill=True, align='C', ln=1)
pdf.cell(0,3, '', border=0, align='C', fill=True, ln=1)

pdf.cell(0,8,' ', ln=1)

pdf.set_font('OpenSans','B', 14)
pdf.set_fill_color(r= 152, g= 231, b = 245)
pdf.cell(0,10,'TOPIKOK',align='C', border=0, fill=True, ln=1)


pdf.cell(0,5,' ', ln=1)

pdf.set_font('OpenSans','B', 9)
pdf.cell(0,10,'AZ ELMÚLT HÉT TOP TÉMÁI*', align='C', ln=1)
pdf.cell(0,10,' ', ln=1)
pdf.image('I:/MONI/Codes/news-monitoring/dashboard/top10_topik_db.png', x = 10, y = 77, w = 190, h = 95, type = 'PNG')
pdf.set_font('OpenSans', '', 6)
pdf.cell(0,88,'',ln=1)
pdf.cell(0,3,'*Magyar nyelvű hírportálok szövegeiből deep learning módszerrel kinyert leggyakrabban előfroduló témák. A témák címeit mesterséges intelligencia állítja elő.',align='R',ln=1)

pdf.set_font('OpenSans','B', 9)
pdf.cell(0,10,'',ln=1)
pdf.cell(0,10,'RELEVÁNS TÉMÁK SAJTÓBAN VALÓ MEGJELENÉSÉNEK IDŐBELI ALAKULÁSA**', align='C', ln=1)
pdf.cell(0,10,'',ln=1)
pdf.image('I:/MONI/Codes/news-monitoring/dashboard/topic_evolution_db.png', x = 10, y = 198, w = 190, h = ch, type = 'PNG')
pdf.set_font('OpenSans', '', 6)
pdf.cell(0,45,'',ln=1)
pdf.cell(0,3,'**A releváns témák heti aggregálása az utolsó naptól görgetve visszafelé. A                                        utolsó napja részleges, a frissítésig beérkezett adatokat tartalmazza.',align='R')
pdf.set_text_color(r=235, g= 123, b = 120)
pdf.cell(0,3,'                                                                            legfrissebb adatpont                                                                                                                           ',align='R',ln=1)


pdf.set_auto_page_break(0)



pdf.add_page()
pdf.set_text_color(r= 0, g= 0, b = 0)
pdf.set_font('OpenSans','B', 14)
pdf.cell(0,10,'FORINT',align='C', border=0, fill=True, ln=1)
pdf.set_font('OpenSans','B', 9)

pdf.cell(0,6,' ', ln=1)
pdf.cell(0,10,'                      FORINT SZENTIMENT INDEX***                                           FORINT SZENTIMENT INDEX - ELMÚLT 30 NAP****', align='C', ln=1)
pdf.image(r'I:/MONI/Codes/news-monitoring/dashboard/forint_sentiment_db.png', x = 10, y = 38, w = 190, h = 50, type = 'PNG')
pdf.set_font('OpenSans', '', 6)
pdf.cell(0,53,'',ln=1)
pdf.cell(0,3,'***A forint témájú cikkekre deep learning módszerrel számított szentiment értékek napi összegzése.',align='R',ln=1)
pdf.cell(0,3,'****Az adott napon megjelenő cikkekre számított szentiment értékek dobozdiagramjai.',align='R',ln=1)
#ábrák

pdf.cell(0,12,'',ln=1)
pdf.set_font('OpenSans','B', 10)
pdf.cell(0,10,'FORINT TÉMÁJÚ CIKKEK SZÓFELHŐJE AZ ELMÚLT HÉTRŐL', align='C', ln=1)
pdf.cell(0,20,'',ln=1)
pdf.image(r'I:/MONI/Codes/news-monitoring/dashboard/forint_wc1_db.png', x = 10, y = 122, w = 190, h = 40, type = 'PNG')
                


pdf.output(r'I:\MONI\Codes\news-monitoring\dashboard\Data Science Dashboard {}.pdf'.format(str(date.today())), 'F')

print(str(datetime.now())[:-7], ' Email..')


filepath = r"I:\MONI\Codes\news-monitoring\dashboard\Data Science Dashboard {}.pdf".format(str(date.today()))
pdf = pdfium.PdfDocument(filepath)

# render a single page (in this case: the first one)
page = pdf[0]
pil_image = page.render(scale=3).to_pil()
pil_image.save("I:/MONI/Codes/news-monitoring/dashboard/output1.jpg")
src_img = cv2.imread("I:/MONI/Codes/news-monitoring/dashboard/output1.jpg")
rows,cols,_ = src_img.shape
crop_img = src_img[0:int(rows*0.88), 0:cols]
cv2.imwrite("I:/MONI/Codes/news-monitoring/dashboard/output1c.jpg", crop_img)
page = pdf[1]
pil_image = page.render(scale=3).to_pil()
pil_image.save("I:/MONI/Codes/news-monitoring/dashboard/output2.jpg")


#try:
olApp=win32.Dispatch('Outlook.Application')
olNS=olApp.GetNameSpace('MAPI')
googlemail = olApp.CreateItem(0)
googlemail.Subject = 'Data Science Dashboard napi riport'
googlemail.BodyFormat = 1 
attachment1 = googlemail.Attachments.Add("I:/MONI/Codes/news-monitoring/dashboard/output1c.jpg")
attachment2 = googlemail.Attachments.Add("I:/MONI/Codes/news-monitoring/dashboard/output2.jpg")
attachment1.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F", "MyId1")
attachment2.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F", "MyId2")
googlemail.HTMLBody = "<html><body><p>Kedves Címzettek!</p> <p>Küldjük a Data Science napi riportunkat.</p> <p>Üdv,</p> <p>MPP</p> <img src=""cid:MyId1""><img src=""cid:MyId2""></body></html>"
#googlemail.Body = "Kedves Címzettek! \n\nMellékelve küldjük a Google Trends napi riportunkat. \n\nÜdv, \nMPP"""

# attachment1 = googlemail.Attachments.Add("I:/MONI/Codes/google-trends/abrak/thermo_{}.png".format(str(date.today())))
# attachment1.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F", "MyId1")
# googlemail.HTMLBody = "<html><body><p>Kedves Címzettek!</p> <p>Küldjük a Google Trends napi riportunkat.</p> <p>Üdv,</p> <p>MPP</p> <p>Mutatóink a Google keresések relatív mértékét mutatják a megadott keresőszavak vonatkozásában. </p> <p>A mutatók jelenlegi értéke az 5 éves maximumhoz képest: </p> <img src=""cid:MyId1""><img src=""cid:MyId2""></body></html>"
googlemail.Attachments.Add(r'I:\MONI\Codes\news-monitoring\dashboard\Data Science Dashboard {}.pdf'.format(str(date.today())))
googlemail.To = 'pitzm@mnb.hu; kocsisz@mnb.hu; jeneiba@mnb.hu'

googlemail.Send()
print('Data Science Dashboard email sent. Good job. Bye! \U0001f600')

print('Runtime: {}'.format(str(datetime.now() - start_time)[:-6]))

